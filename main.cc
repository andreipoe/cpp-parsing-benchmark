#include <bitset>
#include <chrono>
#include <cmath>
#include <cstring>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include "concurrentqueue.h"
#include "re2/re2.h"

#include "trace.pb.h"

class CommaNumPunct : public std::numpunct<char> {
 protected:
  virtual char do_thousands_sep() const { return ','; }
  virtual std::string do_grouping() const { return "\03"; }
};

struct MemoryRequest {
  int seq, tid, size, bundle_kind;
  bool is_write;
  uint64_t address, pc;
};

using BenchFunction = std::function<size_t(const std::string&)>;

void run_bench(BenchFunction f, std::string_view fname, const std::string& tracefname);
size_t bench_stringstream(const std::string& fname);
size_t bench_regex(const std::string& fname);
size_t bench_re2(const std::string& fname);
size_t bench_strtok(const std::string& fname);
size_t bench_protobof(const std::string& fname);
size_t bench_raw(const std::string& fname);
size_t bench_par_stringstream(const std::string& fname);
size_t bench_par_raw(const std::string& fname);

#define PAR_BULK_SIZE 10

enum RunBits {
  BIT_STRINGSTREAM,
  BIT_REGEX,
  BIT_RE2,
  BIT_STRTOK,
  BIT_PROTOBUF,
  BIT_RAW,
  BIT_PAR_SS,
  BIT_PAR_RAW,

  BIT_COUNT
};


void convert_trace_to_pb(const std::string& fname);
void convert_trace_to_raw(const std::string& fname);
size_t par_producer(const std::string& fname, moodycamel::ConcurrentQueue<std::string>& q,
                    moodycamel::ProducerToken& prod_token);
void par_stringstream_consumer(moodycamel::ConcurrentQueue<std::string>& q,
                               moodycamel::ProducerToken& prod_token, size_t& read);

int main(int argc, char* argv[]) {
  if (argc < 2) {
    std::cout << "Usage: benchmark TRACE-FILE [STRATEGY...]\n";
    std::exit(1);
  }
  const std::string tracefname = argv[1];

  std::bitset<BIT_COUNT> run_strategies;
  if (argc == 2)
    run_strategies.set();
  else {
    for (int i = 2; i < argc; i++) {
      std::string arg = argv[i];
      std::transform(arg.begin(), arg.end(), arg.begin(),
                     [](unsigned char c) { return std::tolower(c); });

      if (arg == "stringstream" || arg == "ss")
        run_strategies.set(BIT_STRINGSTREAM);
      else if (arg == "regex")
        run_strategies.set(BIT_REGEX);
      else if (arg == "re2")
        run_strategies.set(BIT_RE2);
      else if (arg == "strtok")
        run_strategies.set(BIT_STRTOK);
      else if (arg == "protobof" || arg == "pb")
        run_strategies.set(BIT_PROTOBUF);
      else if (arg == "raw")
        run_strategies.set(BIT_RAW);
      else if (arg == "stringtream-parallel" || arg == "stringtream-par" ||
               arg == "ss-par")
        run_strategies.set(BIT_PAR_SS);
      else if (arg == "raw-parallel" || arg == "raw-par")
        run_strategies.set(BIT_PAR_RAW);
    }
  }

  std::cout.imbue({ std::locale(), new CommaNumPunct() });

  if (run_strategies.test(BIT_STRINGSTREAM))
    run_bench(bench_stringstream, "stringstream", tracefname);
  if (run_strategies.test(BIT_REGEX)) run_bench(bench_regex, "regex", tracefname);
  if (run_strategies.test(BIT_RE2)) run_bench(bench_re2, "re2", tracefname);
  if (run_strategies.test(BIT_STRTOK)) run_bench(bench_strtok, "strtok", tracefname);
  if (run_strategies.test(BIT_PROTOBUF)) {
    convert_trace_to_pb(tracefname);
    run_bench(bench_protobof, "protobuf", tracefname);
  }
  if (run_strategies.test(BIT_RAW)) {
    convert_trace_to_raw(tracefname);
    run_bench(bench_raw, "raw-binary", tracefname);
  }
  if (run_strategies.test(BIT_PAR_SS))
    run_bench(bench_par_stringstream, "stringstream-par", tracefname);
  if (run_strategies.test(BIT_PAR_RAW)) {
    convert_trace_to_raw(tracefname);
    run_bench(bench_par_raw, "raw-par", tracefname);
  }

  return 0;
}


void run_bench(BenchFunction f, std::string_view fname, const std::string& tracefname) {
  const auto t_start      = std::chrono::high_resolution_clock::now();
  const size_t items_read = f(tracefname);
  const auto t_end        = std::chrono::high_resolution_clock::now();

  const auto wtime_ms =
      std::chrono::duration<double, std::milli>(t_end - t_start).count();
  const auto wtime_us =
      std::chrono::duration<double, std::micro>(t_end - t_start).count();

  std::cout << std::left << std::setw(16) << fname << ": " << items_read
            << " items read, " << std::setw(8) << std::right << std::fixed
            << std::setprecision(2) << wtime_ms << " ms elapsed ("
            << wtime_us / items_read << " μs/item)\n";
}


// ------ Benchmark functions ------

size_t bench_stringstream(const std::string& fname) {
  std::ifstream tracefile { fname };
  std::istringstream iss;

  size_t read { 0 };
  for (std::string line; std::getline(tracefile, line);) {
    if (line.empty()) continue;

    line.erase(std::remove(line.begin(), line.end(), ','), line.end());

    iss.clear();
    iss.str(line);

    int seq, tid, size, bundle_kind;
    bool is_write;
    uint64_t address, pc;
    iss >> seq >> tid >> bundle_kind >> is_write >> size;
    iss >> std::hex >> address >> pc;

    read++;
  }

  return read;
}

size_t bench_regex(const std::string& fname) {
  using namespace std::string_literals;
  const auto pattern {
    R"((\d+), +(\d+), +([0123467]), +([0-1]), +(\d+), +([0-9a-fx]+), +([0-9a-fx]+))"s
  };
  auto regex = std::regex { pattern };

  std::ifstream tracefile { fname };

  size_t read { 0 };
  std::smatch match {};
  for (std::string line; std::getline(tracefile, line);) {
    if (line.empty()) continue;

    try {
      if (std::regex_search(line, match, regex)) {
        int seq = std::stoi(match[1]), tid = std::stoi(match[2]),
            bundle_kind = std::stoi(match[3]), size = std::stoi(match[5]);
        bool is_write    = std::stoi(match[4]);
        uint64_t address = std::stoi(match[6]), pc = std::stoi(match[7]);
      } else
        throw std::invalid_argument("Malformed trace line: "s + line);
    } catch (std::regex_error& e) {
      std::cout << "Regex error " << e.code() << ": " << e.what() << "\n";
      std::exit(2);
    }

    read++;
  }

  return read;
}

size_t bench_re2(const std::string& fname) {
  using namespace std::string_literals;
  const RE2 pattern {
    R"((\d+), +(\d+), +([0123467]), +([0-1]), +(\d+), +([0-9a-fx]+), +([0-9a-fx]+))"s
  };

  std::ifstream tracefile { fname };

  size_t read { 0 };
  for (std::string line; std::getline(tracefile, line);) {
    int seq, tid, size, bundle_kind;
    int is_write;
    uint64_t address, pc;

    std::string s_address, s_pc;
    bool match = RE2::FullMatch(line, pattern, &seq, &tid, &bundle_kind, &is_write, &size,
                                &s_address, &s_pc);
    address    = std::stoul(s_address, nullptr, 16);
    pc         = std::stoul(s_pc, nullptr, 16);

    if (!match) throw std::invalid_argument("Malformed trace line: "s + line);

    read++;
  }

  return read;
}

size_t bench_strtok(const std::string& fname) {
  std::ifstream tracefile { fname };

  size_t read { 0 };
  for (std::string line; std::getline(tracefile, line);) {
    char* p = std::strtok(line.data(), ", ");
    int seq = std::strtol(p, NULL, 10);

    p       = std::strtok(NULL, ", ");
    int tid = std::strtol(p, NULL, 10);

    p               = std::strtok(NULL, ", ");
    int bundle_kind = std::strtol(p, NULL, 10);

    p             = std::strtok(NULL, ", ");
    bool is_write = std::strtol(p, NULL, 10);

    p        = std::strtok(NULL, ", ");
    int size = std::strtol(p, NULL, 10);

    p              = std::strtok(NULL, ", ");
    size_t address = std::strtoul(p, NULL, 16);

    p         = std::strtok(NULL, ", ");
    size_t pc = std::strtoul(p, NULL, 16);

    read++;
  }

  return read;
}

size_t bench_protobof(const std::string& fname) {
  const auto pbfname { fname.substr(0, fname.rfind('.')) + ".pb" };
  std::ifstream tracefile { pbfname, std::ios::binary };

  scs::Trace trace;
  trace.ParseFromIstream(&tracefile);

  return trace.entries_size();
}

size_t bench_raw(const std::string& fname) {
  const auto rawfname { fname.substr(0, fname.rfind('.')) + ".bin" };
  std::ifstream rawfile { rawfname, std::ios::binary };

  size_t elements;
  rawfile.read(reinterpret_cast<char*>(&elements), sizeof(size_t));
  for (size_t i = 0; i < elements; i++) {
    int seq, tid, size, bundle_kind;
    bool is_write;
    uint64_t address, pc;

    rawfile.read(reinterpret_cast<char*>(&seq), sizeof(int));
    rawfile.read(reinterpret_cast<char*>(&tid), sizeof(int));
    rawfile.read(reinterpret_cast<char*>(&size), sizeof(int));
    rawfile.read(reinterpret_cast<char*>(&bundle_kind), sizeof(int));
    rawfile.read(reinterpret_cast<char*>(&is_write), sizeof(bool));
    rawfile.read(reinterpret_cast<char*>(&address), sizeof(uint64_t));
    rawfile.read(reinterpret_cast<char*>(&pc), sizeof(uint64_t));
  }

  return elements;
}

size_t bench_par_stringstream(const std::string& fname) {
  moodycamel::ConcurrentQueue<std::string> q;
  moodycamel::ProducerToken prod_token { q };
  size_t read { 0 };

  std::thread producer { par_producer, fname, std::ref(q), std::ref(prod_token) };
  std::thread consumer { par_stringstream_consumer, std::ref(q), std::ref(prod_token),
                         std::ref(read) };

  producer.join();
  consumer.join();

  return read;
}

size_t bench_par_raw(const std::string& fname) {
  const auto rawfname { fname.substr(0, fname.rfind('.')) + ".bin" };
  const auto nthreads = std::thread::hardware_concurrency();

  std::ifstream rawfile { rawfname, std::ios::binary };
  size_t elements;
  rawfile.read(reinterpret_cast<char*>(&elements), sizeof(size_t));
  rawfile.close();

  const size_t elements_per_thread = std::ceil(elements / static_cast<double>(nthreads));
  const size_t bytes_per_thread =
      elements_per_thread * (4 * sizeof(int) + sizeof(bool) + 2 * sizeof(uint64_t));

  // std::vector<MemoryRequest> requests(elements);

  std::vector<std::thread> threads;
  threads.reserve(nthreads);
  for (size_t thread_num = 0; thread_num < nthreads; thread_num++) {
    threads.emplace_back([&, thread_num]() {
      std::ifstream rawfile { rawfname, std::ios::binary };
      const size_t offset = sizeof(size_t) + bytes_per_thread * thread_num;

      rawfile.seekg(offset);

      for (size_t i = 0; i < elements_per_thread && !rawfile.eof(); i++) {
        int seq, tid, size, bundle_kind;
        bool is_write;
        uint64_t address, pc;

        // const size_t offset = elements_per_thread * thread_num + i;

        rawfile.read(reinterpret_cast<char*>(&seq), sizeof(int));
        rawfile.read(reinterpret_cast<char*>(&tid), sizeof(int));
        rawfile.read(reinterpret_cast<char*>(&size), sizeof(int));
        rawfile.read(reinterpret_cast<char*>(&bundle_kind), sizeof(int));
        rawfile.read(reinterpret_cast<char*>(&is_write), sizeof(bool));
        rawfile.read(reinterpret_cast<char*>(&address), sizeof(uint64_t));
        rawfile.read(reinterpret_cast<char*>(&pc), sizeof(uint64_t));
      }
    });
  }

  for (auto& t : threads) t.join();

  // std::cout << std::count_if(std::begin(requests), std::end(requests),
  //                            [](const auto& e) { return e.seq != 0; });

  return elements;
}

// ------ Helper functions ------

void convert_trace_to_pb(const std::string& fname) {
  const auto pbfname { fname.substr(0, fname.rfind('.')) + ".pb" };
  std::ifstream existing_pbfile { pbfname };

  if (existing_pbfile.is_open()) return;
  existing_pbfile.close();

  std::cout << "Converting trace file to protobuf format (" << pbfname << ")...\n";
  std::ifstream tracefile { fname };
  std::istringstream iss;
  scs::Trace trace;
  for (std::string line; std::getline(tracefile, line);) {
    if (line.empty()) continue;

    line.erase(std::remove(line.begin(), line.end(), ','), line.end());

    iss.clear();
    iss.str(line);

    int seq, tid, size, bundle_kind;
    bool is_write;
    uint64_t address, pc;
    iss >> seq >> tid >> bundle_kind >> is_write >> size;
    iss >> std::hex >> address >> pc;

    auto entry = trace.add_entries();
    entry->set_seq(seq);
    entry->set_tid(tid);
    entry->set_size(size);
    entry->set_bundle_kind(bundle_kind);
    entry->set_is_write(is_write);
    entry->set_address(address);
    entry->set_pc(pc);
  }

  std::ofstream pbfile { pbfname, std::ios::binary };
  trace.SerializeToOstream(&pbfile);
}


void convert_trace_to_raw(const std::string& fname) {
  const auto rawfname { fname.substr(0, fname.rfind('.')) + ".bin" };
  std::ifstream existing_rawfile { rawfname };

  if (existing_rawfile.is_open()) return;
  existing_rawfile.close();

  std::cout << "Converting trace file to raw binary format (" << rawfname << ")...\n";
  std::ofstream rawfile { rawfname, std::ios::binary };
  size_t read { 0 };
  rawfile.write(reinterpret_cast<const char*>(&read), sizeof(size_t));

  std::ifstream tracefile { fname };
  std::istringstream iss;
  for (std::string line; std::getline(tracefile, line);) {
    if (line.empty()) continue;

    line.erase(std::remove(line.begin(), line.end(), ','), line.end());

    iss.clear();
    iss.str(line);

    int seq, tid, size, bundle_kind;
    bool is_write;
    uint64_t address, pc;
    iss >> seq >> tid >> bundle_kind >> is_write >> size;
    iss >> std::hex >> address >> pc;

    rawfile.write(reinterpret_cast<const char*>(&seq), sizeof(int));
    rawfile.write(reinterpret_cast<const char*>(&tid), sizeof(int));
    rawfile.write(reinterpret_cast<const char*>(&size), sizeof(int));
    rawfile.write(reinterpret_cast<const char*>(&bundle_kind), sizeof(int));
    rawfile.write(reinterpret_cast<const char*>(&is_write), sizeof(bool));
    rawfile.write(reinterpret_cast<const char*>(&address), sizeof(uint64_t));
    rawfile.write(reinterpret_cast<const char*>(&pc), sizeof(uint64_t));

    read++;
  }

  // Update the number of elements in the file once we know how many we've read
  rawfile.seekp(0);
  rawfile.write(reinterpret_cast<const char*>(&read), sizeof(size_t));
}


size_t par_producer(const std::string& fname, moodycamel::ConcurrentQueue<std::string>& q,
                    moodycamel::ProducerToken& prod_token) {
  std::ifstream tracefile { fname };
  size_t read { 0 };
  std::string bulk[PAR_BULK_SIZE];
  int bulk_counter { 0 };

  for (std::string line; std::getline(tracefile, line);) {
    if (line.empty()) continue;

    read++;
    bulk[bulk_counter++] = line;

    if (bulk_counter == PAR_BULK_SIZE) {
      q.enqueue_bulk(prod_token, bulk, bulk_counter);
      bulk_counter = 0;
    }
  }
  if (bulk_counter != 0) q.enqueue_bulk(prod_token, bulk, bulk_counter);

  q.enqueue(prod_token, "");

  return read;
}

void par_stringstream_consumer(moodycamel::ConcurrentQueue<std::string>& q,
                               moodycamel::ProducerToken& prod_token, size_t& read) {
  std::string line;
  std::string bulk[PAR_BULK_SIZE];
  std::istringstream iss;
  size_t received;

  read = 0;
  do {
    while ((received =
                q.try_dequeue_bulk_from_producer(prod_token, bulk, PAR_BULK_SIZE)) == 0)
      ;
    for (size_t i = 0; i < received; i++) {
      line = bulk[i];
      if (line.empty()) break;

      line.erase(std::remove(line.begin(), line.end(), ','), line.end());

      iss.clear();
      iss.str(line);

      int seq, tid, size, bundle_kind;
      bool is_write;
      uint64_t address, pc;
      iss >> seq >> tid >> bundle_kind >> is_write >> size;
      iss >> std::hex >> address >> pc;

      read++;
    }
  } while (!line.empty());
}
