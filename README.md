# C++ Parsing Benchmarks

This repository implements a few strategies to parse [memory trace files produced by ArmIE](https://community.arm.com/developer/tools-software/hpc/b/hpc-blog/posts/emulating-sve-on-armv8-using-dynamorio-and-armie).

The strategies currently covered are:

* C++ stringstreams
* C++11 regex
* Google [re2](https://github.com/google/re2)
* A manual implementation using C `strtok`
* A manual implementation using binary I/O
* Google [Protocol Buffers](https://developers.google.com/protocol-buffers/docs/overview)
* A parallel implementation using [a concurrent queue](https://github.com/cameron314/concurrentqueue/tree/v1.0.1) to link a reader thread with a parser thread

## Usage

The project is built using Meson:

```
CXX=clang++ meson build
cd build
ninja
```

To run, simply pass your trace file as a CLI argument:

```
./bench memtrace.log
```

A subset of the strategies can be chosen using the remaining CLI arguments (given in any order):

```
./bench memtrace.log strtok stringstream
```
